export type Constructor<T= {}> = new (...args: any[]) => T;
export type F0Return<T> = () => T;
export interface IMemoizable<T> {
  __memo__: Partial<T>;
}

/**
 * @description
 * documentation purpose
 */
export type Memoized<T> = T & { readonly '_fakeMemoizedAttrs': unique symbol };

export function Memoize<T, K extends keyof T>(target: IMemoizable<T>, key: K, cb: F0Return<T[K]>): T[K] {
  // tslint:disable-next-line:no-string-literal
  const cached = target['__memo__'][key];
  if (cached !== undefined) { return cached as T[K]; } else {
    const result = cb();
    target.__memo__[key] = result;
    return result;
  }
}

export function MemoizeV2<T, K extends keyof T>(target: Partial<T>, key: K, cb: F0Return<T[K]>): Memoized<T[K]> {
  const cached = target[key];
  if (cached !== undefined) { return cached as Memoized<T[K]>; } else {
    const result = cb();
    target[key] = result;
    return result as Memoized<T[K]>;
  }
}

export class MemoizeV3<Store> {
  #store: Partial<Store> = {};
  constructor() {}

  memoize<K extends (keyof Store)>(key: K, cb: F0Return<Store[K]>): Memoized<Store[K]> {
    return MemoizeV2(this.#store, key, cb);
  }

}

class Memoizable {
  private __memo__: { foo?: string } = {};

  foo(): string {
    return MemoizeV2(this.__memo__, 'foo', () => 'bar');
  }
}

class BlankMemoizeParent {}
/**
 * disable tslint because typeof mixin class is kinda hard to hardcode.
 * @param superclass
 */
/* tslint:disable */
export function mixMemoize<Klass extends Constructor, MemoizableProperty>(superclass?: Klass){
/* tslint:enable */
  const parent = superclass === undefined ? BlankMemoizeParent : superclass;
  return class extends parent {

    /* tslint:disable */
    __memo__: Partial<MemoizableProperty> = {};
    /* tslint:enable */

    memoize<S extends keyof MemoizableProperty>(key: S, cb: () => MemoizableProperty[S]): MemoizableProperty[S] {
      const cached = this.__memo__[key];
      if (cached !== undefined) {
        return cached as MemoizableProperty[S];
      } else {
        const result = cb();
        this.__memo__[key] = result;
        return result;
      }
    }
  };
}
