export declare type Constructor<T = {}> = new (...args: any[]) => T;
export declare type F0Return<T> = () => T;
export interface IMemoizable<T> {
    __memo__: Partial<T>;
}
/**
 * @description
 * documentation purpose
 */
export declare type Memoized<T> = T & {
    readonly '_fakeMemoizedAttrs': unique symbol;
};
export declare function Memoize<T, K extends keyof T>(target: IMemoizable<T>, key: K, cb: F0Return<T[K]>): T[K];
export declare function MemoizeV2<T, K extends keyof T>(target: Partial<T>, key: K, cb: F0Return<T[K]>): Memoized<T[K]>;
export declare class MemoizeV3<Store> {
    #private;
    constructor();
    memoize<K extends (keyof Store)>(key: K, cb: F0Return<Store[K]>): Memoized<Store[K]>;
}
/**
 * disable tslint because typeof mixin class is kinda hard to hardcode.
 * @param superclass
 */
export declare function mixMemoize<Klass extends Constructor, MemoizableProperty>(superclass?: Klass): {
    new (): {
        __memo__: Partial<MemoizableProperty>;
        memoize<S extends keyof MemoizableProperty>(key: S, cb: () => MemoizableProperty[S]): MemoizableProperty[S];
    };
    new (...args: any[]): {
        __memo__: Partial<MemoizableProperty>;
        memoize<S extends keyof MemoizableProperty>(key: S, cb: () => MemoizableProperty[S]): MemoizableProperty[S];
    };
};
//# sourceMappingURL=index.d.ts.map