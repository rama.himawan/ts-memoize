"use strict";
var __classPrivateFieldGet = (this && this.__classPrivateFieldGet) || function (receiver, state, kind, f) {
    if (kind === "a" && !f) throw new TypeError("Private accessor was defined without a getter");
    if (typeof state === "function" ? receiver !== state || !f : !state.has(receiver)) throw new TypeError("Cannot read private member from an object whose class did not declare it");
    return kind === "m" ? f : kind === "a" ? f.call(receiver) : f ? f.value : state.get(receiver);
};
var _MemoizeV3_store;
Object.defineProperty(exports, "__esModule", { value: true });
exports.mixMemoize = exports.MemoizeV3 = exports.MemoizeV2 = exports.Memoize = void 0;
function Memoize(target, key, cb) {
    // tslint:disable-next-line:no-string-literal
    const cached = target['__memo__'][key];
    if (cached !== undefined) {
        return cached;
    }
    else {
        const result = cb();
        target.__memo__[key] = result;
        return result;
    }
}
exports.Memoize = Memoize;
function MemoizeV2(target, key, cb) {
    const cached = target[key];
    if (cached !== undefined) {
        return cached;
    }
    else {
        const result = cb();
        target[key] = result;
        return result;
    }
}
exports.MemoizeV2 = MemoizeV2;
class MemoizeV3 {
    constructor() {
        _MemoizeV3_store.set(this, {});
    }
    memoize(key, cb) {
        return MemoizeV2(__classPrivateFieldGet(this, _MemoizeV3_store, "f"), key, cb);
    }
}
exports.MemoizeV3 = MemoizeV3;
_MemoizeV3_store = new WeakMap();
class Memoizable {
    constructor() {
        this.__memo__ = {};
    }
    foo() {
        return MemoizeV2(this.__memo__, 'foo', () => 'bar');
    }
}
class BlankMemoizeParent {
}
/**
 * disable tslint because typeof mixin class is kinda hard to hardcode.
 * @param superclass
 */
/* tslint:disable */
function mixMemoize(superclass) {
    /* tslint:enable */
    const parent = superclass === undefined ? BlankMemoizeParent : superclass;
    return class extends parent {
        constructor() {
            super(...arguments);
            /* tslint:disable */
            this.__memo__ = {};
        }
        /* tslint:enable */
        memoize(key, cb) {
            const cached = this.__memo__[key];
            if (cached !== undefined) {
                return cached;
            }
            else {
                const result = cb();
                this.__memo__[key] = result;
                return result;
            }
        }
    };
}
exports.mixMemoize = mixMemoize;
//# sourceMappingURL=index.js.map