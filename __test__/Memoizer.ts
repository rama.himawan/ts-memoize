import { expect } from 'chai';
import mocha from 'mocha';
import mlog from 'mocha-logger';
import { mixMemoize } from '../src';
interface IX {
  memoizedIncrement: number;
}
class Parent {}
class X extends mixMemoize<typeof Parent, IX>(Parent){
  private counter: number = 0;

  getCounter(): number {
    return this.counter;
  }

  memoizedIncrement(): number {
    return this.memoize('memoizedIncrement', () => {
      this.counter++;
      return this.counter;
    });
  }
}

describe('.memoize', () => {
  it('only triggered once', () => {
    const x = new X();
    /* tslint:disable */
    console.log(x);
    mlog.log(`x.counter = ${x.getCounter()} == 0`);
    expect(x.getCounter()).to.eq(0);
    x.memoizedIncrement();
    mlog.log(`x.counter = ${x.getCounter()} == 1`);
    expect(x.getCounter()).to.eq(1);
    x.memoizedIncrement();
    mlog.log(`x.counter = ${x.getCounter()} == 1`);
    expect(x.getCounter()).to.eq(1);
  });
});